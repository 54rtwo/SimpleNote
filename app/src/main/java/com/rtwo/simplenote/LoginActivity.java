package com.rtwo.simplenote;

import android.app.Activity;
import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class LoginActivity extends Activity {

    private WebView LoginPage;
    //
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        LoginPage = (WebView) findViewById(R.id.loginpage);
        LoginPage.getSettings().setJavaScriptEnabled(true);//允许js脚本
        LoginPage.getSettings().setBuiltInZoomControls(true);//允许缩放
        LoginPage.loadUrl("file:///android_asset/html/login.html");
        LoginPage.setWebViewClient(new SimpleNoteClient());
    }

    private class SimpleNoteClient extends WebViewClient {

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }
    }
}
